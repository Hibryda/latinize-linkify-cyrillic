# latinize-linkify-cyrillic

This tiny package allows for transliteration of Russian and Ukrainian cyrillic letters into latin characters and constructing snake-case links. Optionally, it can output just the transliteration, yet the primary purpose is to return links.

## Usage

The only available function `latinize`, available as named export and the default one, accepts two parameters:
- __\<string\>__ text to transliterate
- __\<boolean\>__ linkification switch, defaults to `true`

```javascript

import { latinize } from '@hibryda/latinize-linkify-cyrillic'

const text = 'Input text containing cyrillic letters / Введите текст, содержащий кириллические буквы'

const link = latinize(text) 
const transcription = latinize(text,false) 

console.log(link)
// output:
// input-text-containing-cyrillic-letters-vvedite-tekst-soderzhaschiy-kirillicheskie-bukvy

console.log(transcription)
// output:
// Input text containing cyrillic letters / Vvedite tekst, soderzhaschiy kirillicheskie bukvy

```

