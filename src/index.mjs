import { substitutions } from './substitutions.mjs'

const latinize = (str, linkify = true) => {
  let s = substitutions.letters
  if (linkify) {
    str = str.trim().replace(/[^\sa-zA-Z0-9\u0100-\u017F\u0400-\u04FF]/gi, "").replace(/\s{1,}/gi, '-').toLowerCase().replace(/зг/g, 'zgh')

  } else {
    s = { ...substitutions.capitals, ...s }
    str = str.replace(/Зг/g, 'Zgh').replace(/зг/g, 'zgh')
  }
  let result = ''
  for (let i = 0; i < str.length; i++) {
    result += s[str[i]] || s[str[i]] === '' ? s[str[i]] : str[i]
  }

  return result
}

export default latinize
export { latinize }